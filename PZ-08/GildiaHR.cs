﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
    public class GildiaHR : EventArgs
    {
        public Mag mag { get; set; }

        public GildiaHR(Mag m)
        {
            this.mag = m;
        }

        public string dodany()
        {
            return string.Format("Mag {1} zostal dodany do gildi oto jego dane \n {2}", mag.imie, mag.ToString());
        }
        public string wydalony()
        {
            return string.Format("Mag {1} zostal wydalony z gildi oto jego dane \n {2}", mag.imie, mag.ToString());
        }
    }
}
