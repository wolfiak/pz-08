﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
    public partial class Gildia
    {

        public  void listaWszystkichMagow()
        {
            Console.WriteLine("Lista magow alfabetycznie po imieniu");
            lista.OrderBy(k => k.imie).ToList().ForEach(i => Console.WriteLine(i.ToString()));

        }
        public  void listaDoswiadczonyck(int poziom)
        {
            Console.WriteLine($"Lista doswiadczonych powyzej poziomu {poziom}");
            lista.OrderBy(k => k.poziom).Where(i => i.poziom > poziom).ToList().ForEach(p => Console.WriteLine(p.ToString()));
        }

        public  void poInteligencji(int poziom)
        {
            Console.WriteLine("Pozim z inteligencja");
            lista.OrderByDescending(k => k.Inteligencja).Where(i => i.Inteligencja > 20 && i.poziom <= poziom).ToList().ForEach(k => Console.WriteLine(k.ToString()));
        }
        public  void sumaPunktowMany()
        {
            Console.WriteLine("Ilosc many");
            int liczba = lista.Where(k => k.poziom > 2 && k.Zrecznosc > 10).Sum(p => p.Mana.Aktualne);
            Console.WriteLine("Ilosc many: " + liczba);
        }
        public  void arsenal(int ilosc)
        {
            Console.WriteLine("Najwiekszy aresnal");
            lista.OrderByDescending(k => k.Ksiega.Count).Where(k => k.Ksiega.Count > ilosc).Select(i => new {
                i.imie,
                iloscCzarow = i.Ksiega.Count,
                i.Mana.Aktualne
            }).ToList().ForEach(i => Console.WriteLine(i.ToString()));
        }
        public  void wszechstorny()
        {
            Console.WriteLine("Lista wszechstronnych magow");
            lista.Select(m => new { m.imie, m.poziom, srednia=((m.Sila * m.Zrecznosc * m.Inteligencja)/3) }).OrderByDescending(m => m.srednia).ToList().ForEach(m =>
            {
                Console.WriteLine($"Mag  {m.imie} o poziomie {m.poziom} posiada srednia {m.srednia}");
            });

        }
        public  void badAss()
        {
            Console.WriteLine("Mag z najwieksza iloscia czarow");
            //lista.Where(m => m.Ksiega.Count > 0).Max(m => m.Ksiega.Count);
            lista.Where(m => m.Ksiega.Count >= lista.Max(k => k.Ksiega.Count)).ToList().ForEach(m =>
            {
                Console.WriteLine($"Mag {m.ToString()}");
            });
        }
        public  void wszystkieCzary()
        {
            Console.WriteLine("Wyswietlanie wszystkich czarow");
            lista.SelectMany(k => k.Ksiega, (m, k) => new { k.Nazwa }).Distinct().ToList().ForEach(m =>
            {
                Console.WriteLine($"Czar {m.Nazwa} ");
            });

        }
        public  void typyCzarow()
        {
            Console.WriteLine("Typy czarow");
            lista.SelectMany(m => m.Ksiega, (m, k) => new { k }).Distinct().Where(m => m.k is CzarOfensywny).ToList().ForEach(m =>
                {
                    Console.WriteLine(m.GetType().Name + ":"+m.k.Nazwa);
                });
        }
        public  void gandalf(string imie)
        {
            Console.WriteLine($"Czary {imie}");
            lista.Single(m => m.imie ==imie).Ksiega.Where(k => k is CzarOfensywny).ToList().ForEach(k =>
             {
                 Console.WriteLine($"Czar {k.Nazwa}");
             });

            //lista.Single(m => m.Ksiega.Any(k => k is CzarOfensywny));
        }
        public  void ileKazdegoTypu()
        {
            lista.SelectMany(m => m.Ksiega, (m, k) => new { k, m.Ksiega.Count }).GroupBy(m => m.k.GetType(), m=> m.Count,(m,k)=> new { Nazwa=m, ilosc=k.Count() }).ToList().ForEach(k=>
            {
                Console.WriteLine($"Nazwa: {k.Nazwa}: {k.ilosc}");
            });
        }
        public void konkretnieTypy(string imie)
        {
            lista.Where(k=> k.imie==imie).SelectMany(m => m.Ksiega, (m, k) => new { k, m.Ksiega.Count }).GroupBy(m => m.k.GetType(), m => m.Count, (m, k) => new { Nazwa = m, ilosc = k.Count() }).ToList().ForEach(k =>
            {
                Console.WriteLine($"Czar: {k.Nazwa}: {k.ilosc}");
            });
        }
        public  void poterzni()
        {
            lista.OrderByDescending(m => m.PunktyDoswiadczenia).Select(k => new { k.imie, k.poziom, k.PunktyDoswiadczenia }).Skip(1).Take(2).ToList().ForEach(k =>
              {
                  Console.WriteLine($"Mag: {k.imie} Poziom: {k.poziom} punkty: {k.PunktyDoswiadczenia}");
              });
        }
        public  void gotowi()
        {
            Console.WriteLine("Magowie sa gotowi do gry ?: {0}",lista.All(k => k.Zycie.Aktualne == k.Zycie.Maksymalne && k.Mana.Aktualne == k.Mana.Maksymalne));
        }
        public  void ktosUmarl()
        {
            Console.WriteLine("Czy ktos umral ?: {0}", lista.Any(k => k.Zycie.Aktualne <= 0));
        }
        public void doMijsji()
        {
            lista.OrderByDescending(k => k.poziom).SelectMany(m => m.Odpornosci, (m, o) => new { m, o }).Select(k => new { mag=k.m, liczba=k.m.Odpornosci.Sum(s=> (int)s) }).Distinct().Take(3).ToList().ForEach(k =>
               {
                   Console.WriteLine($"Imie: {k.mag.imie } Poziom:  {k.mag.poziom} Suma odpornosci: {k.liczba}");
               });

        }
        public  void zapytanie2(Expression<Func<Mag, int>> sortowanie, Expression<Func<Mag, bool>> warunek)
        {
            lista.OrderByDescending(sortowanie.Compile()).Where(warunek.Compile()).ToList().ForEach(k =>
            {
                Console.WriteLine($"{k.imie}  -  {k.Sila}");
            });

        }
        public void zapytanie3(Func<List<Mag>> metoda)
        {
            metoda().ForEach(k=>
            {
                Console.WriteLine($"{k.imie}  -  {k.Sila}");
            });
        }
        public void zapytanie4(silni s, List<Mag> m)
        {
          
                s(m).ForEach(k =>
                {
                    Console.WriteLine($"{k.imie}  -  {k.Sila}");

                });
            
        }

    }
}
