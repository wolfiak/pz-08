﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
    public abstract class Czar
    {
        public string Nazwa { get; set; }
        public int KosztMany { get; set; }
        public int Cooldown { get; set; }

        public override int GetHashCode()
        {
            return this.Nazwa.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            var it = obj as Czar;
            if(it == null)
            {
                return false;
            }
            return this.Nazwa.Equals(it.Nazwa);
        }
    }
}
