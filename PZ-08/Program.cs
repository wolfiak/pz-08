﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
    class Program
    {
        static void Main(string[] args)
        {
            Gildia gildia = new Gildia(new List<Mag>
            {
                new Mag
                {
                   imie="zbyszek",
                   Inteligencja=11,
                   Ksiega=new KsiegaCzarow
                   {
                       new CzarOfensywny
                       {
                           KosztMany=10,
                           Cooldown=10,
                           Nazwa="Kamehameja",
                           Obrazenia=10
                       }
                   },
                   Mana=new Mana
                   {
                       Aktualne=10,
                       Maksymalne=10
                   },
                   Zycie=new Zycie
                   {
                       Aktualne=10,
                       Maksymalne=10
                   },
                   Odpornosci=new List<Odpornosci>{
              
                       Odpornosci.mroz,
                       Odpornosci.trucizna
                   },
                   poziom=10,
                   PunktyDoswiadczenia=10,
                   Sila=10,
                   ZadawaneObrazenia=10,
                   Zrecznosc=15
                   
                       
                  
                },
                new Mag
                {
                   imie="fred",
                   Inteligencja=50,
                   Ksiega=new KsiegaCzarow
                   {
                       new CzarOfensywny
                       {
                           KosztMany=10,
                           Cooldown=10,
                           Nazwa="Kamehameja",
                           Obrazenia=10
                       },
                       new CzarDefensywny
                       {
                           KosztMany=20,
                           Cooldown=10,
                           Nazwa="Lur",
                           Obrona=12
                       },
                       new CzarUzdrawiajacy
                       {
                           KosztMany=20,
                           Cooldown=10,
                           Nazwa="Hil",
                           PunktyZycia=40
                       }
                   },
                   Mana=new Mana
                   {
                       Aktualne=30,
                       Maksymalne=30
                   },
                   Zycie=new Zycie
                   {
                       Aktualne=15,
                       Maksymalne=15
                   },
                    Odpornosci=new List<Odpornosci>{
                       Odpornosci.ogien,
                       Odpornosci.mroz,
                       Odpornosci.trucizna
                   },
                   poziom=25,
                   PunktyDoswiadczenia=25,
                   Sila=12,
                   ZadawaneObrazenia=10,
                   Zrecznosc=15



                },
                new Mag
                {
                   imie="Alfred",
                   Inteligencja=50,
                   Ksiega=new KsiegaCzarow
                   {
                       new CzarOfensywny
                       {
                           KosztMany=10,
                           Cooldown=10,
                           Nazwa="Kamehameja",
                           Obrazenia=10
                       },
                       new CzarDefensywny
                       {
                           KosztMany=20,
                           Cooldown=10,
                           Nazwa="Lur",
                           Obrona=12
                       },
                       new CzarUzdrawiajacy
                       {
                           KosztMany=20,
                           Cooldown=10,
                           Nazwa="Hil",
                           PunktyZycia=40
                       },
                        new CzarOfensywny
                       {
                           KosztMany=30,
                           Cooldown=10,
                           Nazwa="Boom",
                           Obrazenia=20
                       }
                   },
                   Mana=new Mana
                   {
                       Aktualne=30,
                       Maksymalne=30
                   },
                   Zycie=new Zycie
                   {
                       Aktualne=15,
                       Maksymalne=15
                   },
                   Odpornosci=new List<Odpornosci>{
                       Odpornosci.ogien,
                       Odpornosci.mroz,
                       Odpornosci.trucizna
                   },
                   poziom=30,
                   PunktyDoswiadczenia=25,
                   Sila=12,
                   ZadawaneObrazenia=10,
                   Zrecznosc=15



                }
                ,
                new Mag
                {
                   imie="rookie",
                   Inteligencja=4,
                   Ksiega=new KsiegaCzarow
                   {
                       new CzarOfensywny
                       {
                           KosztMany=10,
                           Cooldown=10,
                           Nazwa="slaby",
                           Obrazenia=1
                       }
                   },
                   Mana=new Mana
                   {
                       Aktualne=5,
                       Maksymalne=5
                   },
                   Zycie=new Zycie
                   {
                       Aktualne=5,
                       Maksymalne=5
                   },
                    Odpornosci=new List<Odpornosci>{
                       Odpornosci.fizyczne
                   },
                   poziom=1,
                   PunktyDoswiadczenia=1,
                   Sila=5,
                   ZadawaneObrazenia=5,
                   Zrecznosc=5



                }

            });
           
            gildia.onHrStuff += new EventHandler<GildiaHR>(Przyjecie);
            gildia.onHrStuff += new EventHandler<GildiaHR>(Wydalenie);

            // gildia.wyswietl();
            // gildia

            // gildia.listaWszystkichMagow();
            // gildia.listaDoswiadczonyck(4);
            // gildia.poInteligencji(50);
            //gildia.sumaPunktowMany();
            //gildia.arsenal(2);
            //gildia.wszechstorny();
            //gildia.badAss();
            // gildia.wszystkieCzary();
            // gildia.typyCzarow();
            // gildia.gandalf("Alfred");
            //gildia.ileKazdegoTypu();
            // gildia.konkretnieTypy("Alfred");
            //  gildia.poterzni();
            //gildia.gotowi();
            //gildia.ktosUmarl();
            // gildia.doMijsji();
            //  gildia.zapytanie1(k=> k.Sila,p=> p.Sila > 10);
            // gildia.zapytanie2(k => k.Sila, p => p.Sila > 10);
            /*gildia.zapytanie3(()=>
            {
                return gildia.lista.OrderByDescending(k => k.Sila).Where(k => k.Sila > 10).ToList();
            });
            */
            //  gildia.zapytanie4(magowie, gildia.lista);
            //gildia.zapytanieBezLinq();
            gildia.operatoryZapytania();
            Console.ReadKey();
        }
        public static List<Mag> magowie(List<Mag> lista)
        {
            return lista.OrderByDescending(k => k.Sila).Where(k => k.Sila > 10).ToList();
        }
        public static void Przyjecie(object sender, GildiaHR hr)
        {

        }
        public static void Wydalenie(object sender, GildiaHR hr)
        {

        }
     
    }
}
