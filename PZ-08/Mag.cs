﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
  public  class Mag
    {
        public string imie { get; set; }
        public int poziom { get; set; }
        public int PunktyDoswiadczenia { get; set; }
        public int Sila { get; set; }
        public int Zrecznosc { get; set; }
        public int Inteligencja { get; set; }
        public Zycie Zycie { get; set; }
        public Mana Mana { get; set; }
        public int ZadawaneObrazenia { get; set; }
        public List<Odpornosci> Odpornosci { get; set; }
        public KsiegaCzarow Ksiega { get; set; }

        public override string ToString()
        {
            string tmp= string.Format("Imie: {0} Poziom: {1} Punkty Dosiwadczenia: {2} Sila: {3} Zrecznosc: {4} Inteligencja: {5} Punkty zycia aktualne: {6} Punkty zycia maksymalne {7} Mana aktualna {8} Mana maksymalna {9} Obrazenia {10} Odprnosc {11}  ",this.imie,this.poziom,poziom,PunktyDoswiadczenia,Sila,Zrecznosc,Inteligencja,Zycie.Aktualne,Zycie.Maksymalne,Mana.Aktualne,Mana.Maksymalne,ZadawaneObrazenia, Odpornosci);
            Ksiega.ForEach(i =>
            {
                tmp += i.Nazwa + " ";
            });
            return tmp;
        }
    }
    public enum Odpornosci
    {
        fizyczne=10,
        ogien=100,
        mroz=80,
        trucizna=30
    }
}
