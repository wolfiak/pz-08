﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
    public delegate List<Mag> silni(List<Mag> m);
   public static class Junkyard
    {
        public static void zapytanie1(this Gildia g, Expression<Func<Mag,int>> sortowanie,Expression<Func<Mag,bool>> warunek)
        {
             g.lista.OrderByDescending(sortowanie.Compile()).Where(warunek.Compile()).ToList().ForEach(k=>
             {
                 Console.WriteLine($"{k.imie}  -  {k.Sila}");
             });
            
        }
        public static void zapytanieBezLinq(this Gildia g)
        {
            Mag[] tablica = g.lista.ToArray();
            for(int n=0; n < g.lista.Count; n++)
            {
                for(int m = 0; m < g.lista.Count - 1; m++)
                {
                    if(tablica[n].Sila< tablica[m].Sila)
                    {
                        Mag tmp =tablica[n];
                        tablica[n] = tablica[m];
                        tablica[m] = tmp;
                    }
                }
            }
            for(int n = 0; n < g.lista.Count; n++)
            {
                if(tablica[n].Sila > 10)
                {
                    Console.WriteLine($"{tablica[n].imie}  -  {tablica[n].Sila}");
                }
                
            }
           
        }
        public static void operatoryZapytania(this Gildia g)
        {
            var res = from gi in g.lista
                      orderby gi.Sila descending
                      where gi.Sila > 10 select gi;

            foreach(Mag m in res)
            {
                Console.WriteLine($"{m.imie}  -  {m.Sila}");
            }
        }
    }
}
