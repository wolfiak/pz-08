﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_08
{
    

   public partial  class Gildia
    {
        public EventHandler<GildiaHR> onHrStuff;
        public  List<Mag> lista { get; set; }
        public Gildia(List<Mag> lista)
        {
            this.lista = lista;
        }



      

        public void wyswietl()
        {
            lista.ForEach(k =>
            {
                Console.WriteLine(k.ToString());
            });
        }
        public void dodajNowego(Mag m)
        {
            bool lol = lista.Any(x => x.imie == m.imie);
            if (lol)
            {
                throw new Exception($"{m.imie} juz u nas jest!");
            }
            else
            {
                lista.Add(m);
                onHrStuff(this, new GildiaHR(m));
            }
            
        }
        public void usunMaga(string imie)
        {
            bool lol=lista.Any(x => x.imie == imie);
            if (lol)
            {
                Console.WriteLine($"Na pewno chcesz usunac {imie} t/n");
                string odp=Console.ReadLine();
                if (odp == "t")
                {
                    lista.RemoveAll(x => x.imie == imie);
                    onHrStuff(this, new GildiaHR(lista.First(k => k.imie == imie)));
                }
            }
            else
            {
                throw new Exception("NIe ma takiego maga ");
            }
           
           
        }

        public void Subskrubuj(Mag m)
        {
            Console.WriteLine("Dodaje "+m.imie);
           // onPrzyjecie(m);
        }

        public void Usun(string m)
        {
            Console.WriteLine("Usuwam "+m);
           // onUsuniecie(m);
        }


    }
  
}
